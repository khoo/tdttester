#!/bin/env python3
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaConfiguration import AllConfigFlags, MainServicesConfig
from AthenaConfiguration.ComponentFactory import CompFactory

flags = AllConfigFlags.initConfigFlags()
flags.Input.Files = ['DAOD.pool.root']

flags.fillFromArgs()
flags.lock()

cfg= MainServicesConfig.MainServicesCfg(flags)

from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
tdtcfg = TrigDecisionToolCfg(flags)
tdt = tdtcfg.getPrimary()
cfg.merge(tdtcfg)

print("EDM ver", flags.Trigger.EDMVersion)
print("Trigger config source", flags.Trigger.triggerConfig)

cfg.merge( PoolReadCfg(flags) )

cfg.addEventAlgo(
    CompFactory.TDTTestAlg(
        "tdttest",
        tdt=tdt,
        triggers=[
            "HLT_j420_pf_ftf_preselj225_L1J100",
            "HLT_e26_lhtight_ivarloose_L1EM22VHI",
            "HLT_mu26_ivarmedium_L1MU14FCH",
            "HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1jJ85p0ETA21_3jJ40p0ETA25",
            "HLT_j30_0eta290_020jvt_boffperf_pf_ftf_L1J20"
        ]
    )
)

cfg.run(10)
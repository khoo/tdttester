#include "TDTTestAlg.h"

TDTTestAlg::TDTTestAlg(const std::string &name,
                                 ISvcLocator* loc):
  AthAlgorithm(name, loc),
  m_trigDecisionTool()
{
  declareProperty("tdt", m_trigDecisionTool, "trigger decision tool");
  declareProperty("triggers", m_trigList, "trigger selection list");
}

StatusCode TDTTestAlg::initialize() {
  ATH_CHECK(m_trigDecisionTool.retrieve());

  return StatusCode::SUCCESS;
}

StatusCode TDTTestAlg::execute() {

  for (const std::string& trig : m_trigList) {
    ATH_MSG_INFO("Trigger " << trig << " passed ? " << m_trigDecisionTool->isPassed(trig) );
  }

  return StatusCode::SUCCESS;
}

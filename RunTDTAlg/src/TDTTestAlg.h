#ifndef TDT_TEST_ALG_H
#define TDT_TEST_ALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include <TrigDecisionInterface/ITrigDecisionTool.h>

class TDTTestAlg: public AthAlgorithm
{
public:
  TDTTestAlg(const std::string& name, ISvcLocator* loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;

private:
    ToolHandle<Trig::ITrigDecisionTool> m_trigDecisionTool;
    std::vector<std::string> m_trigList;
};


#endif
